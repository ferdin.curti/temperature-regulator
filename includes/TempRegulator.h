
#ifndef TEMP_REGULATOR_H
#define TEMP_REGULATOR_H

#include <functional>

namespace Regulators {
    enum class cmd {
        NO_CMD,
        CMD_ON,
        CMD_OFF
    };

    /* Using a struct instead of a std::pair to avoid confusion about
       which is the cooling cmd and which is the heating cmd */
    typedef struct {
        cmd cooling;
        cmd heating;
    } commands;

    /**
     * \brief This class is offering an interface for setting up a min and max temperature,
     *        and every time a temperature update is received a decision is taken on
     *        activating/deactivating the heating and/or the cooling devices.
     *
     * \note If no heating device is present and it is only required to command the cooling device,
     *       this can simply be achieved by not setting any heating command, same is true for the
     *       opposite (no cooling device, only heating required -> do not set a cooling command)
     *
     * \author Ferdin Curti
     */
    class TempRegulator
    {
    public:
        TempRegulator(float tempMin, float tempMax);

        /**
         * \brief Setting up a heating callback function, this provides to the api a way of triggering
         *        the heating system with a bool parameter as input/command.
         * \param heating  Represents the command given to the heating device: heating ON = true; heating OFF = false;
        */
        void setHeatingCmd(std::function<void(bool)> heating);

        /**
         * \brief Setting up a cooling callback function, this provides to the api a way of triggering
         *        the cooling system with a bool parameter as input/command.
         * \param cooling  Represents the command given to the cooling device: cooling ON = true; cooling OFF = false;
        */
        void setCoolingCmd(std::function<void(bool)> cooling);

        /**
         * \brief The user needs to provide updates of the temperature via this update function (in most cases it will
         * probably be just a thread with a cyclic task to read and forward the temperature from a sensor every <amount> ms)
         * \param currentTemp  Represents the current temperature from the sensor, based on this action will be taken (if necessary).
        */
        void update(float currentTemp);

        /**
         * \return Returns the current min temperature.
        */
        float getMinTemp() const;
        /**
         * \brief sets the new min temperature.
         * \param temp the new value for min temperature.
        */
        void setMinTemp(float temp);
        /**
         * \return Returns the current max temperature.
        */
        float getMaxTemp() const;
        /**
         * \brief sets the new max temperature.
         * \param temp the new value for max temperature.
        */
        void setMaxTemp(float temp);
        /**
         * \return Returns weather the heating device is/should be active.
         * \note This can help troubleshooting, or even be used for implementing a different approach
         *       for activating/deactivating the heating device if callback methods cannot be provided.
        */
        bool isHeating() const;
        /**
         * \return Returns weather the cooling device is/should be active.
         * \note This can help troubleshooting, or even be used for implementing a different approach
         *       for activating/deactivating the cooling device if callback methods cannot be provided.
        */
        bool isCooling() const;

    protected:
        /**
         * \brief The inheriting classes have to implement the logic by which the heating and cooling devices are activated/deactivated.
         *        They have access to min and max temperatures and to the currentTemp, which was just updated before this method is called
         * \return Returns a struct containing the 2 commands, one for the heating device and one for the cooling device.
        */
        virtual commands regulationMethod() const = 0;

        float tempMin;
        float tempMax;
        float currentTemp;

    private:
        bool isCoolingOn = false;
        bool isHeatingOn = false;

        std::function<void(bool)> heatingCmd;
        std::function<void(bool)> coolingCmd;
    };
}

#endif /* TEMP_REGULATOR_H */
