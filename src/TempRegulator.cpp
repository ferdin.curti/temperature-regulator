#include "TempRegulator.h"


namespace Regulators {
    TempRegulator::TempRegulator(float min, float max):tempMin(min), tempMax(max)
    {
    }

    void TempRegulator::setHeatingCmd(std::function<void(bool)> heating)
    {
        heatingCmd = heating;
    }
    void TempRegulator::setCoolingCmd(std::function<void(bool)> cooling)
    {
        coolingCmd = cooling;
    }
    float TempRegulator::getMinTemp() const
    {
        return tempMin;
    }
    void TempRegulator::setMinTemp(float temp)
    {
        tempMin = temp;
    }
    float TempRegulator::getMaxTemp() const
    {
        return tempMax;
    }
    void TempRegulator::setMaxTemp(float temp)
    {
        tempMax = temp;
    }
    bool TempRegulator::isHeating() const
    {
        return isHeatingOn;
    }
    bool TempRegulator::isCooling() const
    {
        return isCoolingOn;
    }

    void TempRegulator::update(float temp)
    {
        currentTemp = temp;
        auto command = regulationMethod();
        if (command.cooling == cmd::CMD_OFF)
        {
            isCoolingOn = false;
            if (coolingCmd != nullptr)
            {
                coolingCmd(isCoolingOn);
            }
        }
        else if (command.cooling == cmd::CMD_ON)
        {
            isCoolingOn = true;
            if (coolingCmd != nullptr)
            {
                coolingCmd(isCoolingOn);
            }
        }

        if (command.heating == cmd::CMD_OFF)
        {
            isHeatingOn = false;
            if (heatingCmd != nullptr)
            {
                heatingCmd(isHeatingOn);
            }
        }
        else if (command.heating == cmd::CMD_ON)
        {
            isHeatingOn = true;
            if (heatingCmd != nullptr)
            {
                heatingCmd(isHeatingOn);
            }
        }
    }
}
