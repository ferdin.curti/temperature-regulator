
#ifndef HISTERESIS_REGULATOR_H
#define HISTERESIS_REGULATOR_H

#include "TempRegulator.h"

namespace Regulators {
    /**
     * \brief This class is extending the TempRegulator, offering a histeresis type of regulation,
     *        meaning the heating will turn on when the temperature (minTemp - histDelta) is reached
     *        and will turn back off when the temperature (minTemp + histDelta) is reached,
     *        cooling will turn on when the temperature (maxTemp + histDelta) is reached and will
     *        turn back off when the temperature (maxTemp - histDelta) is reached.
     * \note Unless specified the histDelta value will default to 1.0
     * \author Ferdin Curti
     */
    class HisteresisRegulator: public TempRegulator
    {
    public:
        HisteresisRegulator(float tempMin, float tempMax);

        /**
         * \brief sets the new histeresis delta.
         * \param delta the new value for the histeresis delta.
        */
        void setHisteresisDelta(float delta);
        /**
         * \return Returns the current value of the histeresis delta.
        */
        float getHisteresisDelta() const;

    private:
        commands regulationMethod() const override;

        float histDelta = 1.0;
    };
}

#endif /* HISTERESIS_REGULATOR_H */
