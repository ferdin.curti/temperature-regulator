#include "HisteresisRegulator.h"


namespace Regulators {
    HisteresisRegulator::HisteresisRegulator(float min, float max):TempRegulator(min, max)
    {
    }
    void HisteresisRegulator::setHisteresisDelta(float delta)
    {
        histDelta = delta;
    }
    float HisteresisRegulator::getHisteresisDelta() const
    {
        return histDelta;
    }

    commands HisteresisRegulator::regulationMethod() const
    {
        commands ret{cmd::NO_CMD, cmd::NO_CMD};
        if (currentTemp < (tempMin - histDelta)) // start heating
        {
            ret.heating = cmd::CMD_ON;
        }
        else if (currentTemp > (tempMin + histDelta)) // stop heating 
        {
            ret.heating = cmd::CMD_OFF;
        }

        if (currentTemp > (tempMax + histDelta)) // start cooling
        {
            ret.cooling = cmd::CMD_ON;
        }
        else if (currentTemp < (tempMax - histDelta)) // stop cooling 
        {
            ret.cooling = cmd::CMD_OFF;
        }

        return ret;
    }
}