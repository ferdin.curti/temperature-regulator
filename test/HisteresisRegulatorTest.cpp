#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "HisteresisRegulator.h"

using namespace Regulators;

TEST(HisteresisRegulator, SettersAndGettersTest)
{
    HisteresisRegulator reg {5.0, 35.0};
    ASSERT_FLOAT_EQ(1.0, reg.getHisteresisDelta());

    reg.setHisteresisDelta(3.14);
    ASSERT_FLOAT_EQ(3.14, reg.getHisteresisDelta());
}

/* As the temperature goes between <min temp - delta> and <min temp + delta>
 * no heating cmd is sent, thus making sure to keep whichever command was previously sent
 * if heating was off and the room is cooling down, keep it off
 * if heating was on and the room is heating up, keep it on
 */
TEST(HisteresisRegulator, HisteresisHeatingCommands)
{
    bool heating = false;
    bool cooling = false;
    int heatingCnt = 0;
    int coolingCnt = 0;
    HisteresisRegulator reg {5.0, 35.0};
    ASSERT_FLOAT_EQ(1.0, reg.getHisteresisDelta());
    reg.setHeatingCmd([&heating, &heatingCnt](bool on){ heating = on; heatingCnt++; });
    reg.setCoolingCmd([&cooling, &coolingCnt](bool on){ cooling = on; coolingCnt++; });

    // 4.1 is within the no command range for heating,
    // but in the turn cooling off range -> calling coolingCmd with false
    reg.update(4.1);
    ASSERT_EQ(false, reg.isHeating());
    ASSERT_EQ(false, reg.isCooling());
    ASSERT_EQ(false, heating);
    ASSERT_EQ(false, cooling);
    ASSERT_EQ(0, heatingCnt);
    ASSERT_EQ(1, coolingCnt);

    // 3.9 is past the min temp - delta histerezis, turning on the heating
    reg.update(3.9);
    ASSERT_EQ(true, reg.isHeating());
    ASSERT_EQ(false, reg.isCooling());
    ASSERT_EQ(true, heating);
    ASSERT_EQ(false, cooling);
    ASSERT_EQ(1, heatingCnt);
    ASSERT_EQ(2, coolingCnt);

    // temp starts going back up, we keep the previous command
    // and don't give a new command untill we are passed min temp + delta
    reg.update(4.5);
    reg.update(4.9);
    reg.update(5.9);
    ASSERT_EQ(true, reg.isHeating());
    ASSERT_EQ(false, reg.isCooling());
    ASSERT_EQ(true, heating);
    ASSERT_EQ(false, cooling);
    ASSERT_EQ(1, heatingCnt);
    ASSERT_EQ(5, coolingCnt);

    // Once we're passed the min temp + delta we can stop the heating
    reg.update(6.5);
    ASSERT_EQ(false, reg.isHeating());
    ASSERT_EQ(false, reg.isCooling());
    ASSERT_EQ(false, heating);
    ASSERT_EQ(false, cooling);
    ASSERT_EQ(2, heatingCnt);
    ASSERT_EQ(6, coolingCnt);
}

/* As the temperature goes between <max temp - delta> and <max temp + delta>
 * no cooling cmd is sent, thus making sure to keep whichever command was previously sent
 * if cooling was off and the room is heating up, keep it off
 * if cooling was on and the room is cooling down, keep it on
 */
TEST(HisteresisRegulator, HisteresisCoolingCommands)
{
    bool heating = false;
    bool cooling = false;
    int heatingCnt = 0;
    int coolingCnt = 0;
    HisteresisRegulator reg {5.0, 35.0};
    ASSERT_FLOAT_EQ(1.0, reg.getHisteresisDelta());
    reg.setHeatingCmd([&heating, &heatingCnt](bool on){ heating = on; heatingCnt++; });
    reg.setCoolingCmd([&cooling, &coolingCnt](bool on){ cooling = on; coolingCnt++; });

    // 35.9 is within the no command range for cooling,
    // but in the turn heating off range -> calling heatingCmd with false
    reg.update(35.9);
    ASSERT_EQ(false, reg.isHeating());
    ASSERT_EQ(false, reg.isCooling());
    ASSERT_EQ(false, heating);
    ASSERT_EQ(false, cooling);
    ASSERT_EQ(1, heatingCnt);
    ASSERT_EQ(0, coolingCnt);

    // 36.7 is past the max temp + delta histerezis, turning on the cooling
    reg.update(36.7);
    ASSERT_EQ(false, reg.isHeating());
    ASSERT_EQ(true, reg.isCooling());
    ASSERT_EQ(false, heating);
    ASSERT_EQ(true, cooling);
    ASSERT_EQ(2, heatingCnt);
    ASSERT_EQ(1, coolingCnt);

    // temp starts going back down, we keep the previous command
    // and don't give a new command untill we are passed max temp - delta
    reg.update(36.2); // still above max + delta -> sends another cooling on cmd
    reg.update(35.4);
    reg.update(34.2);
    ASSERT_EQ(false, reg.isHeating());
    ASSERT_EQ(true, reg.isCooling());
    ASSERT_EQ(false, heating);
    ASSERT_EQ(true, cooling);
    ASSERT_EQ(5, heatingCnt);
    ASSERT_EQ(2, coolingCnt);

    // Once we're passed the max temp - delta we can stop the cooling
    reg.update(33.5);
    ASSERT_EQ(false, reg.isHeating());
    ASSERT_EQ(false, reg.isCooling());
    ASSERT_EQ(false, heating);
    ASSERT_EQ(false, cooling);
    ASSERT_EQ(6, heatingCnt);
    ASSERT_EQ(3, coolingCnt);
}
