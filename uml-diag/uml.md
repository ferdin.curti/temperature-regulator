```mermaid
classDiagram
class TempRegulator {
    <<Abstract>>
    # minTemp : float
    # maxTemp : float
    # currentTemp : float
    - heatingCmd : std::function<>
    - coolingCmd : std::function<>
    - isHeatingOn : bool
    - isCoolingOn : bool
    + TempRegulator(float min, float max)
    + setHeatingCmd(std::function<> callback) void
    + setCoolingCmd(std::function<> callback) void
    + update(float temp) void
    + getMinTemp() float
    + setMinTemp(float min) void
    + getMaxTemp() float
    + setMaxTemp(float max) void
    + isHeating() bool
    + isCooling() bool
    ~pure virtual~ # regulationMethod()* commands
}

class HysteresisRegulator{
    + HysteresisRegulator(float min, float max)
    # regulationMethod() commands
    + setHisteresisDelta(float delta) void
    + getHisteresisDelta() float
    - histDelta : float
}

class cmd{
    <<enumeration>>
    NO_CMD
    CMD_ON
    CMD_OFF
}

class commands{
    + cooling : cmd
    + heating : cmd
}

commands --> cmd
TempRegulator ..> commands
TempRegulator <|-- HysteresisRegulator
```