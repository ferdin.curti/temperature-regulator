
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "TempRegulator.h"

using namespace Regulators;
class TestTempRegulator : public TempRegulator
{
public:
    TestTempRegulator(float min, float max) : TempRegulator(min, max) {}
    MOCK_METHOD(commands, regulationMethod, (), (const, override));
};

TEST(TempRegulator, SettersAndGettersTest)
{
    TestTempRegulator reg {5.0, 35.0};
    ASSERT_FLOAT_EQ(5.0, reg.getMinTemp());
    ASSERT_FLOAT_EQ(35.0, reg.getMaxTemp());

    reg.setMinTemp(6.78);
    ASSERT_FLOAT_EQ(6.78, reg.getMinTemp());
    reg.setMaxTemp(32.57);
    ASSERT_FLOAT_EQ(32.57, reg.getMaxTemp());
}

TEST(TempRegulator, UpdateAndCallbacks)
{
    bool heating = false;
    bool cooling = false;
    int heatingCnt = 0;
    int coolingCnt = 0;
    TestTempRegulator reg {5.0, 35.0};
    reg.setHeatingCmd([&heating, &heatingCnt](bool on){ heating = on; heatingCnt++; });
    reg.setCoolingCmd([&cooling, &coolingCnt](bool on){ cooling = on; coolingCnt++; });

    ASSERT_EQ(false, reg.isHeating());
    ASSERT_EQ(false, reg.isCooling());

    EXPECT_CALL(reg, regulationMethod())
        .WillOnce(::testing::Return(commands{cmd::NO_CMD, cmd::NO_CMD}));
    reg.update(10);
    ASSERT_EQ(false, reg.isHeating());
    ASSERT_EQ(false, reg.isCooling());
    ASSERT_EQ(false, heating);
    ASSERT_EQ(false, cooling);
    ASSERT_EQ(0, heatingCnt);
    ASSERT_EQ(0, coolingCnt);
}

TEST(TempRegulator, UpdateHeatingOnOFF)
{
    bool heating = false;
    bool cooling = false;
    int heatingCnt = 0;
    int coolingCnt = 0;
    TestTempRegulator reg {5.0, 35.0};
    reg.setHeatingCmd([&heating, &heatingCnt](bool on){ heating = on; heatingCnt++; });
    reg.setCoolingCmd([&cooling, &coolingCnt](bool on){ cooling = on; coolingCnt++; });

    ASSERT_EQ(false, reg.isHeating());
    ASSERT_EQ(false, reg.isCooling());

    // Turns on the heating
    EXPECT_CALL(reg, regulationMethod())
        .WillOnce(::testing::Return(commands{cmd::NO_CMD, cmd::CMD_ON}));
    reg.update(10);
    ASSERT_EQ(true, reg.isHeating());
    ASSERT_EQ(false, reg.isCooling());
    ASSERT_EQ(true, heating);
    ASSERT_EQ(false, cooling);
    ASSERT_EQ(1, heatingCnt);
    ASSERT_EQ(0, coolingCnt);

    // NO_CMD -> nothing changes
    EXPECT_CALL(reg, regulationMethod())
        .WillOnce(::testing::Return(commands{cmd::NO_CMD, cmd::NO_CMD}));
    reg.update(15);
    ASSERT_EQ(true, reg.isHeating());
    ASSERT_EQ(false, reg.isCooling());
    ASSERT_EQ(true, heating);
    ASSERT_EQ(false, cooling);
    ASSERT_EQ(1, heatingCnt);
    ASSERT_EQ(0, coolingCnt);

    // Turns off the heating
    EXPECT_CALL(reg, regulationMethod())
        .WillOnce(::testing::Return(commands{cmd::NO_CMD, cmd::CMD_OFF}));
    reg.update(20);
    ASSERT_EQ(false, reg.isHeating());
    ASSERT_EQ(false, reg.isCooling());
    ASSERT_EQ(false, heating);
    ASSERT_EQ(false, cooling);
    ASSERT_EQ(2, heatingCnt);
    ASSERT_EQ(0, coolingCnt);
}

TEST(TempRegulator, UpdateCoolingOnOFF)
{
    bool heating = false;
    bool cooling = false;
    int heatingCnt = 0;
    int coolingCnt = 0;
    TestTempRegulator reg {5.0, 35.0};
    reg.setHeatingCmd([&heating, &heatingCnt](bool on){ heating = on; heatingCnt++; });
    reg.setCoolingCmd([&cooling, &coolingCnt](bool on){ cooling = on; coolingCnt++; });

    ASSERT_EQ(false, reg.isHeating());
    ASSERT_EQ(false, reg.isCooling());

    // Turns on the cooling
    EXPECT_CALL(reg, regulationMethod())
        .WillOnce(::testing::Return(commands{cmd::CMD_ON, cmd::NO_CMD}));
    reg.update(20);
    ASSERT_EQ(false, reg.isHeating());
    ASSERT_EQ(true, reg.isCooling());
    ASSERT_EQ(false, heating);
    ASSERT_EQ(true, cooling);
    ASSERT_EQ(0, heatingCnt);
    ASSERT_EQ(1, coolingCnt);

    // NO_CMD -> nothing changes
    EXPECT_CALL(reg, regulationMethod())
        .WillOnce(::testing::Return(commands{cmd::NO_CMD, cmd::NO_CMD}));
    reg.update(15);
    ASSERT_EQ(false, reg.isHeating());
    ASSERT_EQ(true, reg.isCooling());
    ASSERT_EQ(false, heating);
    ASSERT_EQ(true, cooling);
    ASSERT_EQ(0, heatingCnt);
    ASSERT_EQ(1, coolingCnt);

    // Turns off the cooling
    EXPECT_CALL(reg, regulationMethod())
        .WillOnce(::testing::Return(commands{cmd::CMD_OFF, cmd::NO_CMD}));
    reg.update(10);
    ASSERT_EQ(false, reg.isHeating());
    ASSERT_EQ(false, reg.isCooling());
    ASSERT_EQ(false, heating);
    ASSERT_EQ(false, cooling);
    ASSERT_EQ(0, heatingCnt);
    ASSERT_EQ(2, coolingCnt);
}
